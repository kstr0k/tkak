# tkak

## _Kakoune tmux + terminal, desktop and AppImage integration_

`tkak` starts [`kakoune`](https://github.com/mawww/kakoune) in a `tmux` session running inside a terminal emulator &mdash; in an integrated fashion:
- any `tmux` commands that create panes / windows automatically connect to the editor (instead of dumping you to a shell prompt)
- inside `kakoune`, `:split` and `:vsplit` open panes just as in `vim`
- if the terminal emulator supports it, `:tabnew` opens an editor tab
- running `tkak` again from a manually-opened terminal tab takes over that tab and connects to the same editor

Additionally, the installation script adds
- a `tkak.desktop` file for desktop environments
- an `AppRun` script that makes the `kakoune` folder AppImage (or [`fusemnt+run`]()) ready

## Installing

Clone this repo and run `./setup --help`
- `./setup --dir=/opt/kakoune kak2tkak` (or wherever `kakoune` lives; if `kak` is in `$PATH`, or in `/opt/kak[oune]`, it will be detected). This will augment an existing `kakoune` install.
- `./setup --dir=... [--link-dir=...] addlinks` will install appropriate links to make `tkak` known to the system (under `$HOME/.local` if not specified, or use `/usr/local` for system-wide)
- `./setup --dir=... appimage` will create an AppImage self-mounting binary which can be called with `kak` or `tkak` as the first argument, or can be symlinked `busybox`-style (`ln -s kak.appimage tkak`)
- `setup --no-interactive` wrecks (well, hopefully configures) your system without confirming
- `setup --uninstall ...` undoes (almost) whatever `setup ...` did

## Usage

If you use a desktop environment and have run `setup addlinks`, there should be an entry for `Kakoune (tkak)` in your menu. More likely you will first test `tkak` from the command line. `TKAK_CFG=/dev/null tkak --help`[^1] (abbreviated) says
```
tkak [OPTION].. [[--] <kak --help args..>]; tkak options:
--fface="Monospace" --fsize="12"
--kak="" --kak-init="" --session=""
--tkakrc="/home/user1000/.config/kak/tkak/tkakrc"
--tmux="" --tmux-init="" --tmux-prefix="C-]"
--xterm=""
--xterm-exec-argv="true"
--xterm-tab=""
--xterm-try="st urxvt xterm xfce4-terminal gnome-terminal sakura lxterminal terminator konsole kitty sh"
Default --xterm-try='st urxvt xterm xfce4-terminal gnome-terminal sakura lxterminal terminator konsole kitty sh'
See kak --help
```

All options command-line arguments are also configurable via environment variables (`TKAK_FFACE`, `TKAK_TMUX_PREFIX` etc), or permanently by setting these variables in `~/.config/kak/tkak/tkak.cfg` (`tkak` goes through the usual `$KAKOUNE_CONFIG_DIR` / `$XDG_CONFIG_DIR/kak` song & dance, as documented in `kakoune`, and sources `tkak.cfg`). Most important to get started:
- try `--fface='?'` to select a font semi-interactively
- change `--xterm-try=` to a single value if your preferred terminal emulator is among those listed; otherwise, you will have to muck around with `--xterm` and `--xterm-exec-argv` (does the terminal execute a single string or the rest of the command line)
- `tkak --xterm-try=sh` simply runs `tmux` without any separate x-terminal
- `--tmux-prefix` is set to `C-]` (`Control-]`, which can also be typed on most keyboards as `C-5`); for example `C-] :` shows the tmux command line, `C-] ?` shows `tmux` help, `C-] "/%` splits horizontally/vertically etc.
- `tkakrc` will be created if missing &mdash; by default it only contains `:split`, `:vsplit` and `:tabnew`, but feel free to change it
- once those values work for you from the command line, save them in an appropriate `tkakrc`, and *then* you can start `tkak` from the desktop menu

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license

[^1]: `--help` prints options after reading configuration / environment early on, so if you have a `tkak.cfg`, you won't see the naked default values, unless you override *that* first
